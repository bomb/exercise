# check how many chickens are there.
puts 'I will now count my chichens:'

# hens have 30.
puts 'Hens', 25 + 30 / 6
# roosters have 97.
puts 'Roosters', 100 - 25 * 3 % 4

# the number of eggs.
puts 'Now I will count the eggs:'

# number : 7
puts 3 + 2 + 1 - 5 + 4 % 2 - 1 / 4 + 6

# is it true
puts 'Is it true that 3 + 2 < 5 - 7?'

# false
puts 3 + 2 < 5 - 7

# 5
puts 'what is 3 + 2?', 3 + 2
# -2
puts 'what is 5 - 7?', 5 - 7

# it's false
puts "Oh, that's why it's false."

# it's false
puts 'How about some more.'

# true
puts 'Is it greater?', 5 > -2
# true
puts 'Is it greater or equal?', 5 >= -2
# false
puts 'Is it less or equal?', 5 <= -2
